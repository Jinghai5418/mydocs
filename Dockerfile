FROM squidfunk/mkdocs-material

MAINTAINER Jinghai5418@live.cn

COPY docs /usr/share/mydocs/docs/
COPY material /usr/share/mydocs/material/
COPY mkdocs.yml /usr/share/mydocs/

WORKDIR /usr/share/mydocs

#CMD [ "bash" ]
