# Install CentOS VM

## Download

[Cent OS 7.8](https://vault.centos.org/7.8.2003/isos/x86_64/CentOS-7-x86_64-DVD-2003.torrent)

[Cent OS 7.9](http://isoredirect.centos.org/centos/7/isos/x86_64/)

[Cent OS 最新版本](https://www.centos.org/download/)

## Create a VM

Create a VM with below type

![Operation System Type](./Images/CENTOS-VM-CREATE.PNG)

## Change VM Network

Install CentOS 8 in VMWare, with a network adapter in Bridge mode

![Configuration](./Images/CENTOS-VM-CONFIG.PNG)

## Installation & Partitioning

Load the downloaded Cent-OS ISO file to virtual DVD or the VM, start the VM, start to install the OS, with below partitioning 

| Partition Name | Size                                                                                                      |
| -------------- |-----------------------------------------------------------------------------------------------------------|
| /root | 512 MB                                                                                                    |
| swap | 16 GB (16 384 MB, if allocate 4 GB memory for the VM)<br/>or<br/>8 GB (8 192 MB, in case of 8GB memory for the VM) |
| / |                                                                                                           |


## Change NIC setting

```shell
vi /etc/sysconfig/network-scripts/ifcfg-ens33
```

```properties
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=ens33
UUID=dbcdca8c-7cb4-4b25-8a18-7684e9deefdc
DEVICE=ens33
ONBOOT=yes
NM_CONTROLLED=yes
IPADDR=192.168.0.169
NETMASK=255.255.255.0
GATEWAY=192.168.0.1
DNS1=192.168.0.1
DNS2=8.8.8.8
DNS3=114.114.114.114
```

```shell
systemctl restart NetworkManager
```

## Change prompt (Optional)

```shell
vi /etc/profile.d/env.sh
```

```properties
PS1="[\u@\H \W] "
```

```shell
source /etc/profile.d/env.sh
```

or you can just run:

```shell
echo "PS1=\"[\u@\H \W] \"" > /etc/profile.d/env.sh
source /etc/profile.d/env.sh
```

## Update Yum repositories

```shell
cd /etc/yum.repos.d
cat CentOS-AppStream.repo
sed -i 's/mirrorlist/#mirrorlist/g' CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' CentOS-*
cat CentOS-AppStream.repo

yum update -y
```

## Install docker

```shell
curl -sSL https://get.daocloud.io/docker | sh

systemctl start docker
systemctl enable docker
```

Or with below commands:

```shell
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io --allowerasing

```

## Install Docker-Compose

```shell
curl -L https://get.daocloud.io/docker/compose/releases/download/v2.6.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

## Install unzip

```shell
yum -y install unzip
```
