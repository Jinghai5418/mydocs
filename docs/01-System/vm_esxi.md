# 在 VMware ESXI 虚拟机中追加存储

首先给 ESXI 虚拟机添加磁盘，然后进入 ESXI 虚拟机管理界面添加存储

![Add a disk](Images/ESXI_ADD_HD.PNG)
![Create a storage](Images/ESXI_CREATE_STORAGE.PNG)