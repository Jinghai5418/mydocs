# Windows 系统与软件

## 相关软件包下载地址

Windows 操作系统及相关软件可以从 <https://msdn.itellyou.cn> 下载。

Windows 操作系统最新版本可以从 <https://next.itellyou.cn/Original> 下载。

U 启动软件可以从 <http://uqidong.njshengyuanli.com> 下载。

## USB启动盘制作

安装 U启动，运行 U启动，切换到 ISO模式，选择导出ISO文件的名称与路径，点击生成按钮，导出U启动的WIN10 PE：

![Extract Win10 PE](./Images/EXTRACT-WIN10-PE.PNG)

运行Rufus将此 PE 写入U盘：

![Write the PE to the U Drive](./Images/WRITE-PE.PNG)

在U盘根目录下新建ISO和GHO两个文件夹，将Windows原版安装光盘的ISO文件拷贝到ISO：

![Copy the ISO/GHO file to the U Drive](./Images/COPY-ISO.PNG)

## USB启动与磁盘分区

进入CMOS设置，用U盘启动电脑：

![Boot with U drive](./Images/BOOT-OPTION.PNG)

<a id="partitioning"> U盘启动后，进入Win 10 PE，打开傲梅分区助手：</a>

![Disk Partition](./Images/DISK-PARTITION.PNG)

随后加载安装程序镜像光盘：

![Load the DVD Image](./Images/LOAD-ISO.PNG)

然后就可以打开虚拟光驱，查看内容，找到安装文件，双击执行它：

![Setup](./Images/START-SETUP.PNG)

## L358 打印机切换网络

如果无线路由器或无线AP不支持 WPS 功能（Wi-Fi Protected Setup），切换无线连接有点麻烦，可参照以下步骤来设置。

[产品主页](https://www.epson.com.cn/Apps/tech_support/SupportProduct.aspx?p=30574)

[说明书](https://www.epson.com.cn/resource/Download/Product/358%E5%8D%95%E9%A1%B5.pdf)

[驱动下载](https://www.epson.com.cn/Apps/tech_support/GuideDriveContent.aspx?ColumnId=30574&ArticleId=38068)

找到相关软件包

![Copy the ISO/GHO file to the U Drive](./Images/EPSON-FIND-INSTALL-NAVI.PNG)

用USB数据线连接电脑和打印机，运行该程序进行设置变更：

![Copy the ISO/GHO file to the U Drive](./Images/EPSON-INSTALL-NAVI.PNG)

随后根据提示进行相关设置：

![Folow the step](./Images/EPSON-INSTALL-NAVI-1.PNG)
![Folow the step](./Images/EPSON-INSTALL-NAVI-2.PNG)
![Folow the step](./Images/EPSON-INSTALL-NAVI-3.PNG)
![Folow the step](./Images/EPSON-INSTALL-NAVI-4.PNG)

## WSL 安装

可选择安装 Ubuntu 20.04.4 LTS

## VM Ware 安装

为了兼容 WSL 与 Docker，需安装VMware Workstation 15.5.6 以上版本，百度网盘里找。

## WTG 制作

参考以下网页：

[萝卜头网站](https://bbs.luobotou.org/thread-761-1-1.html)

[WTG GitHub 网页](https://github.com/nkc3g4/wtg-assistant)

[WTG 论坛](http://bbs.wuyou.net/forum.php?mod=viewthread&tid=330493&extra=page%3D1)

## Win 8 还原

分区同以上 <a href="#partitioning"> USB启动与磁盘分区 </a> 一节所述。

![Folow the step](./Images/WIN8-RECOVERY-1.PNG)
![Folow the step](./Images/WIN8-RECOVERY-2.PNG)
![Folow the step](./Images/WIN8-RECOVERY-3.PNG)
![Folow the step](./Images/WIN8-RECOVERY-4.PNG)
![Folow the step](./Images/WIN8-RECOVERY-5.PNG)

或者选择安装：

![Folow the step](./Images/WIN8-INSTALL.PNG)
