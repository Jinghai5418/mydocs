# Gradle

## 多项目构建

[参考网页](https://blog.csdn.net/dshf_1/article/details/105412625)

Gradle对构建模块化项目提供了强大的支持，在Gradle中每一个模块都是一个项目，称之为Gradle多项目构建技术，多项目构建实现了项目模块化，降低代码耦合度，增加内聚性，本篇文章将简单介绍如何使用Gradle进行多项目构建。

### 多项目构建概要

Gradle整个项目是由一个根目录和多个模块的子目录构成，在根目录下新建build.gradle构建脚本，使用gradle projects命令查看所有参与构建的项目：

示例中使用的多项目层次结构如下图，定义了3个子项目：

![](gradle_images/gradle-project-hierarchy.png)

①settings文件介绍：使用过Gradle构建项目的都应该知道这个文件，该文件声明了项目层次结构所需的配置。默认情况下，这个文件被命名为settings.gradle，并且和根项目的build.gradle文件放在一起。简单配置settings.gradle文件：

![](gradle_images/gradle-settings-file.png)

②settings API的理解：Gradle在组装构建之前，会创建一个Settings实例，Settings接口是settings文件的直接表示，它的主要作用就是添加Project实例参与多项目构建，下图显示了Settings接口的相关方法：

![](gradle_images/gradle-settings-api.png)

我们可以在settings.gradle文件中直接使用这些相关的API进行编码，例如调用了include(String)方法将子项目添加到多项目的构建中。

③settings执行的阶段：在介绍task的使用时说到了构建过程的三个生命周期：初始化、配置和执行。那么是在哪个阶段解析执行settings文件中的代码的呢？可以想到的是，它肯定是在Project实例配置之前进行实例化的：

![](gradle_images/gradle-settings-execution.png)

④如何找到settings文件：只要根项目或者任何子项目的目录中包含构建文件，Gradle就运行你从相应的位置运行构建。 Gradle是如何知道一个子项目是一个多项目构建的一部分的呢？它需要找到settings文件，这个文件指示了子项目是否包含在多项目构建中。步骤：①首先Gradle在与当前目录同层次的master目录下搜索settings文件，②找不到则在父目录下查找settings文件，下图分别对应这两种情况：

![](gradle_images/gradle-finding-seetings-file.png)

⑤项目的两种布局：分层布局和平面布局，android项目中使用的就是分层布局。对于平面布局来说，要想包括与根项目同一嵌套层次的项目，需要使用includeFlat方法，下图分别对应的是分层布局和平面布局：

![](gradle_images/gradle-project-layout.png) 

### 子项目的构建文件

每个子项目的构建需要在子项目的目录下新建一个build.gradle构建脚本，该目录树如下图：

![](gradle_images/gradle-build-files.png)

①根项目构建脚本：

![](gradle_images/gradle-root-project-build-file.png)

②子项目构建脚本：一般在子项目构建脚本中不声明任何代码也是可以的，除非有一些其它的需求，例如web子项目的构建脚本如下：

![](gradle_images/gradle-child-project-build-file.png)

### 重命名构建脚本文件

标准的Gradle构建文件名是build.gradle，但是也许有必要使用一些更具有表达性的名称来命名构建文件。想使用自定义的构建脚本文件名，关键在于settings文件。假如项目结构如下图：

![](gradle_images/gradle-renamed-build-files.png)

![](gradle_images/gradle-use-renamed-build-files.png)

以上简单介绍多项目构建的原理，settings文件如何实现基本的配置，如何是一个项目作为子项目添加到构建中等。还是那句话，知其所以然能够让你得心应手。
