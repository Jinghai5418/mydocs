# Java

# Reference

[Reference](https://www.runoob.com/java/java-hashmap.html)

## Java8 Map 的 compute、computeIfPresent、computeIfAbsent 区别以及使用场景

结论： compute方法 = computeIfPresent方法 + computeIfAbsent方法

一、compute(参数一、参数二)方法：

```
hashmap.compute(K key, BiFunction remappingFunction)
```

参数一：指定的key
参数二：接口函数（k,v）

(修改数据) 原始Map指定的key值存在，函数接口返回不会空，则用接口函数返回的结果替换key的value
(删除数据) 原始Map指定的key值存在，函数接口返回为空，删除key对应的数据
(添加数据) 原始Map指定的key值不存在，函数接口返回为不空，则添加一条数据到Map，key：指定的key value：函数返回的结果

二、computeIfPresent（参数一、参数二）

```
hashmap.computeIfPresent(K key, BiFunction remappingFunction)
```

参数一：指定的key
参数二：接口函数（k,v）

(修改数据) 原始Map指定的key值存在，函数接口返回不会空，则用接口函数返回的结果替换key的value
(删除数据) 原始Map指定的key值存在，函数接口返回为空，删除key对应的数据

三、computeIfAbsent（参数一、参数二）

```Java
hashmap.computeIfAbsent(K key, Function remappingFunction)
```

参数一：指定的key
参数二：接口函数（k）

(添加数据) 原始Map指定的key值不存在，函数接口返回为不空，则添加一条数据到Map，key：指定的key value：函数返回的结果
