# Summary

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## How to run it

```shell
mkdocs serve -a 0.0.0.0:8008
```

```shell
docker run -it --name mydocs -p 8008:8000 mydocs
```

```shell
docker-compose up -d
```

Or in this way:

![Run/Debug Configuration](docs/00-Images/run-docker-compose.png)

## Publish to GitHub Pages

Now, when a new commit is pushed to either the master or main branches, the static site is automatically built and deployed. Push your changes to see the workflow in action.

If the GitHub Page doesn't show up after a few minutes, go to the settings of your repository and ensure that the [publishing source branch](https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site) for your GitHub Page is set to gh-pages.

Your documentation should shortly appear at <username>.github.io/<repository>.

If you prefer to deploy your project documentation manually, you can just invoke the following command from the directory containing the mkdocs.yml file:

```shell
mkdocs gh-deploy --force
```

## Publish to GitHub Pages

Now, when a new commit is pushed to master, the static site is automatically built and deployed. Commit and push the file to your repository to see the workflow in action.

Your documentation should shortly appear at <username>.gitlab.io/<repository>.
